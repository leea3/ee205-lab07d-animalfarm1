///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file config.h
/// @version 1.0
///
/// @brief header file with debug #DEFINES and common return numbers
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date   10 Mar 2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define ERROR (1)
#define NOERROR (0)
#define BAD_CAT (-1)
#define MAX_NAME1 "1234567890123456789012345678901234567890123456789"
#define MAX_NAME2 "DIFFERENT 123456789012345678901234567890123456789"
#define ILLEGAL_NAME "123456789012345678901234567890123456789012345678901"
#define PROGRAM_NAME "animalFarm"
